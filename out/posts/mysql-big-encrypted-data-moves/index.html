<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Sharding Large, Encrypted MySQL Tables | Jeff Minard, Web Developer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link  rel="stylesheet" href="/css/all.v1.min.css" />
    <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="/img/favicon.png">
</head>
<body>

    <header>
        <h1 class="logo"><a href="/"></a></h1>
        <div class="menubutton" id="menubutton"><i class="icon-menu"></i></div>
        <nav id="sitenav">
            <ul>
                
                    <li class="inactive">
                        <a href="/">
                            Home
                        </a>
                    </li>
                
                    <li class="inactive">
                        <a href="/about/">
                            About
                        </a>
                    </li>
                
                    <li class="inactive">
                        <a href="/resume/">
                            Resume
                        </a>
                    </li>
                
            </ul>
        </nav>
        <br class="clear" />
    </header>

    <section>
    <div class="eight columns">

        <h1 class="post_title">Sharding Large, Encrypted MySQL Tables</h1>

        
        <a href="/archive.html"><span class="post_date">Dec. 30</span></a>
        

        <p>Alright, let&#39;s say you have a pretty big database. I&#39;m talking, like, 100GB
for a medium sized table here. (Some tables in excess of 3TB!)</p>
<p>Now let&#39;s say that you&#39;re going to be sharding this dataset. I&#39;m not going to
cover sharding itself in this post, but the concept is pretty simple: we want
to split the data out across multiple servers in an easy to look up fashion. In
this scenario, we&#39;re going to split the data up 1024 ways and do the lookup by
<code>modulo</code> hash of <code>userId</code>.</p>
<p>You&#39;re now faced with a multipart problem: How do you export all this data,
split it up for all the shards, and then load it where it is supposed to go?</p>
<h2 id="data-export">Data Export</h2>
<p>Typically, you&#39;re going to see the advice to use <code>mysqldump</code>. Don&#39;t. It&#39;s not
going to work. For starters, there can be a lot of issues with it timing out,
making sure it doesn&#39;t lock things it shouldn&#39;t, and some other problems.
Primarily, however, it&#39;s default export is SQL statements -- and SQL statements,
while they can be fast to load, they are <em>not</em> the fastest way to load data.</p>
<p>Instead, you should use the <code>SELECT INTO OUTFILE</code> command. It will look
something like this:</p>
<pre class="highlight"><code class="hljs sql"><span class="hljs-operator"><span class="hljs-keyword">SELECT</span>
userId, username, hashedPassword, secretData
<span class="hljs-keyword">INTO</span> OUTFILE <span class="hljs-string">'/path/to/users.csv'</span>
FIELDS TERMINATED <span class="hljs-keyword">BY</span> <span class="hljs-string">','</span> OPTIONALLY ENCLOSED <span class="hljs-keyword">BY</span> <span class="hljs-string">'"'</span> LINES TERMINATED <span class="hljs-keyword">BY</span> <span class="hljs-string">'\n'</span>
<span class="hljs-keyword">FROM</span> <span class="hljs-keyword">schema</span>.users;</span>
</code></pre>
<p>This will create a standard &#39;ol CSV file. However, there&#39;s a problem here. That
<code>secretData</code> column is a binary field with encrypted (or any kind of binary)
data. Binary data can contain any set of characters, including problem
characters like <code>\n</code> or <code>&quot;</code> which will ruin the format of the CSV file and lead
to lost data. Not to mention that most systems expect CSVs to be ascii
plaintext.</p>
<p>So, how do you export this data in a sane CSV? Turns out it&#39;s pretty easy,
just <code>hex()</code> the binary values:</p>
<pre class="highlight"><code class="hljs sql"><span class="hljs-operator"><span class="hljs-keyword">SELECT</span>
userId, username, hashedPassword, hex(secretData)
<span class="hljs-keyword">INTO</span> OUTFILE <span class="hljs-string">'/path/to/users.csv'</span>
FIELDS TERMINATED <span class="hljs-keyword">BY</span> <span class="hljs-string">','</span> OPTIONALLY ENCLOSED <span class="hljs-keyword">BY</span> <span class="hljs-string">'"'</span> LINES TERMINATED <span class="hljs-keyword">BY</span> <span class="hljs-string">'\n'</span>
<span class="hljs-keyword">FROM</span> <span class="hljs-keyword">schema</span>.users;</span>
</code></pre>
<p>This will convert the binary data into its hexidecimal representation which,
in turn, creates a perfectly acceptable CSV file.</p>
<h2 id="data-splitting">Data Splitting</h2>
<p>Now that we have every row exported, we need to split the data up acording to
which shard the row of data will ultimately reside in. So, in our case, if a
row belongs to <code>userId=584685</code> that means <code>584685 % 1024 = 1005</code> the record
should go to shard 1005 (of 1024 total). We need to evaluate every record in
the CSV and put that line into a different CSV which contains records for
the given shard.</p>
<p>I toiled on this for several days, mostly trying to add multi-threading to the
process. Ultimately, however, diskio is the limiting factor and your disk will
never out-perform your CPU. (Maybe you could put this file into <code>/tmpfs</code> and
multi-thread it, but it&#39;s really not worth it.)</p>
<p>So, in order to read the file, parse it, and dump each record into the
appropriate CSV file we turn to the incomprehensible <code>awk</code> linux tool:</p>
<pre class="highlight"><code class="hljs bash">awk -F, <span class="hljs-string">'{x=sprintf("%.4d", $1%1024); print &gt; x".csv" }'</span>
</code></pre>
<p>In short, what this does is pull the first column of data (split by commas),
modulo against 1024, set that to the variable <code>x</code> and then appends the whole
line to <code>x.csv</code>. The <code>sprintf</code> is there so we get <code>0090.csv</code> instead of <code>90.csv</code>
which is a bit nicer when listing the files.</p>
<p>Now, <code>awk</code> will happily run this until it finishes, but as it can take a very
long time to run, I highly recommend you install the <code>pv</code> utility and run the
command like this to get visual progress bar and ETA:</p>
<pre class="highlight"><code class="hljs bash">pv /path/to/users.csv | awk -F, <span class="hljs-string">'{x=sprintf("%.4d", $1%1024); print &gt; x".csv" }'</span>
</code></pre>
<p>Finally, run these commands in a <code>screen</code> session so if you lose your SSH
connection all is not lost.</p>
<h2 id="loading-the-data">Loading The Data</h2>
<p>Great, you&#39;ve got the data all split out, but if you do a normal <code>LOAD DATA INFILE</code>
that <code>hex()</code>d data isn&#39;t that you actually want in the column. Fortuneately,
<code>LOAD DATA INFILE</code> has a trick which lets us get the data imported in the
correct format:</p>
<pre class="highlight"><code class="hljs sql"><span class="hljs-operator"><span class="hljs-keyword">LOAD</span> DATA <span class="hljs-keyword">LOCAL</span> INFILE <span class="hljs-string">'/path/to/shard.csv'</span>
<span class="hljs-keyword">REPLACE</span> <span class="hljs-keyword">INTO</span> <span class="hljs-keyword">TABLE</span> <span class="hljs-keyword">schema</span>.users
FIELDS TERMINATED <span class="hljs-keyword">BY</span> <span class="hljs-string">','</span> OPTIONALLY ENCLOSED <span class="hljs-keyword">BY</span> <span class="hljs-string">'"'</span> LINES TERMINATED <span class="hljs-keyword">BY</span> <span class="hljs-string">'\n'</span>
userId, username, hashedPassword, @secretData
<span class="hljs-keyword">SET</span> secretData = unhex(@secretData);</span>
</code></pre>
<p>This is an interesting extra little feature of <code>LOAD DATA</code> wherein you can
specify each portion of the CSV to go into a column or into a variable. You
place the <code>hex()</code>d data into the <code>@secretData</code> variable and then use the <code>SET</code>
portion of the command to <code>unhex()</code> the value into the <code>secretData</code> column,
thus restoring your binary data.</p>
<h2 id="loading-each-shard">Loading Each Shard</h2>
<p>Getting these CSV&#39;s loaded into each shard, reader, is an exercise I leave in
your hands. I will say some loops, shell commands, and <code>LOAD DATA LOCAL INFILE</code>
are your friend.</p>
<h2 id="result">Result</h2>
<p>The previous data migration system was a program that <code>SELECT</code>ed rows on one
connection from the source, then used a separate connection to the target
to insert them. This method, for the 100GB table had an ETA of <strong>over 120 days</strong>.
The dump/split/load method worked in a grand total of <strong>12 hours.</strong> Most of that
time (~10.5 hours) was spent on the splitting of the file itself. The dump took
40 minutes and the (parallel) load another 45.</p>


        <div class="metadata">
            <span><i class="icon-time"></i>2013-12-30</span>
            
            
        </div>
    </div>
    <br class="clear" />
</section>

    <footer>
        <div class="slate">
            <div class="five columns">
                <h5>Recent Posts</h5>

                <ul>

                
                
                    
                    <li class="">
                        2015-04-10<br/>
                        <a href="/posts/battlestation/">Battlestation</a>
                    </li>
                
                    
                    <li class="">
                        2014-08-01<br/>
                        <a href="/posts/eco-reco-m3-electric-scooter-review/">EcoReco M3 Electric Scooter Review</a>
                    </li>
                
                    
                    <li class="">
                        2014-05-11<br/>
                        <a href="/posts/php-aes-openssl/">PHP: AES Mcrypt &amp; OpenSSL</a>
                    </li>
                

                </ul>

            </div>
            <div class="three columns">
                <h5>Catch me elsewhere:</h5>
                <a href="https://github.com/chuyskywalker"><button class="sb github"><i class="icon-github"></i></button></a>
                <a href="mailto:contactbutton@jrm.cc"><button class="sb envelope"><i class="icon-envelope"></i></button></a>
                <a href="https://www.facebook.com/jeff.minard"><button class="sb facebook"><i class="icon-facebook"></i></button></a>
                <a href="https://www.linkedin.com/in/jeffminard"><button class="sb linkedin"><i class="icon-linkedin"></i></button></a>
            </div>
        </div>
    </footer>

    <script>
    var
        btn = document.getElementById('menubutton'),
        nav = document.getElementById('sitenav');
    btn.onclick = function(){
        nav.style.display = nav.style.display == 'block' ? 'none' : 'block';
    };
    </script>
    

</body>
</html>