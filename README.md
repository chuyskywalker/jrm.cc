# My site

To install deps:

```
sudo docker run -it --rm -v $(pwd):/app:rw node bash -c 'cd /app; npm install'
```

To run dev mode:

```
sudo rm -rf ./out/* && sudo docker run -it --rm -p 9778:9778 -v $(pwd):/app:rw docpad/docpad run
```

To run build:

```
sudo rm -rf ./out/* && sudo docker run -it --rm -p 9778:9778 -v $(pwd):/app:rw docpad/docpad generate --env static
```
