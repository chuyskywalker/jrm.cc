

## My Philosophy

A web site, like any advertisement for a business, is a communication channel to
your customers. If you want to effectively communicate with people your web site
should be three things:

 1. Accessible
 2. Usable
 3. Interactive

### Accessibility

**Accessibility means easy to use anywhere.** With more technologies (PDA's,
Cell Phone, even console gaming systems) hooking into the internet you need your
web site to work across all of these platforms. An accessible web site will.

**Accessibility means better search ranking.** Over 50% of web site traffic
comes from people using search engines like Google.com. When your web site is
accessible it's not only easier for people to read, it's easier for search
engines to read - and that means better rankings.

**Accessibility is the law.** U.S. law requires that all businesses are handicap
accessible - that includes your web site. This means passing certain standards
that almost every web site on the internet does not meet. In fact, many
government web sites still don't pass.

### Usability

**Usability means people can use your site.** While sounding like a joke, far
too many web sites have so much glitz and glamour put into the design of the
site, that people often simply don't know what to do. When "Joe Customer" can't
find the Home button, he's leaving.

**Usability means communicating to the customer.** Most people come to a web
page looking for info. If they don't find it - or start to find it - within 7-20
seconds, they're going to go elsewhere. When your site is usable, your message
will get across to the customer quicker and with more potency.

**Usability means it's easy to update.** If you've had a web site made before,
you'll know that no web site is ever complete. They require upkeep and
maintenance to keep them fresh. Designed with usability in mind, your web site
will be easier and faster to update.

### Interactivity

**Interactivity means communicating with the customer.** Web sites are not print
material. It can change and involve your customers in ways that no other medium
can. A productive web site will not only sell your product, but will keep
selling it through engagement.

**Interactivity does not mean Flash intros.** Flash has it's place on the web -
but it's not in over sized and pointlessly annoying web site intros. Just like
animated GIFs, their existence alone is not enough to justify their use.

## Mix it all together

It is only once you combine all of these three things - accessibility,
usability, and interactivity - into a single product that you will have an
effective website. I strive to bring this unity to every project.